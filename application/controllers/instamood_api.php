<?php if( ! defined('BASEPATH')) exit ('Direct Access Denied');
require(APPPATH.'libraries/REST_Controller.php');
class instamood_api extends REST_Controller{

	/* POST functions */
	/* Accounts Functions */
	
	 public function login_get($username,$password){
		
	 	$data = array('username' => $username,
	 	          'password' => $password,
	 	             'token' => $this->post('token')

	 	 );
		 
		 if(!$credentials = $this->user_model->get(NULL,$data['username'])){
	 	    return $this->response("User not found",403); 
	 	 }
		 
	 	 if($credentials[0]->password != $data['password']){
	 	    return $this->response("Incorrect password",403);
	 	 }
		 
	 	 $this->session->set_userdata(array(
	 	        'username' => $data['username'],
	 	        'email' => $credentials[0]->email,
	 	        'id' => $credentials[0]->id
	 	 )
	 	 );
		 
         $this->response("Logged in",200);
	 	 }

	
	// public function register_post(){
		
	// 	$data = array('username' => $this->post('username'),
	// 			  'password' => $this->post('password'),
	// 			      'name' => $this->post('name'),
	// 			     'email' => $this->post('email')
	// 		);
	// 	$this->user_model->add($data);
	// 	$this->response($data);
	
	// }
	
	public function share_get($user_id, $content, $song_id){
		$data = array(
		'sender_id' => $user_id,
	      'content' => $content,
	      'song_id' => $song_id,
	     'date_shared' => date(
	     'Y-m-d'),
		);

		if(!$this->message_model->add($data)){
			return $this->response('failed');

		}
		return $this->response('shared');
		
	}
	
	public function get_song_get($mood){
		$result = $this->song_model->get(NULL, $mood);
		if($result == false)
		{
			$this->response('No song found');
		}
		else
		{
			// foreach($result as $row)
			// {	
			// 	$this->response($row);
			// }
			$data = json_encode($result);
			$this->response($data);
		}

	}
	
	public function load_profile_get($id){
		
		if($this->user_model->get($id) == false){
			return $this->response('No profile found');
		}
		// $data = json_encode($this->user_model->get($id),NULL);
			

		// $following = json_encode($this->following_model->count_following($id),NULL);
		// 	$this->response($data);

		$raw_info = $this->user_model->get($id);
		$data = array();
		 foreach($raw_info as $info){
		 	$song_id = $this->message_model->get_song($id);
		 	$song = $this->song_model->get($song_id[0]['song_id']);
		    $following_data = $this->following_model->count_following($id);
		    $follower_data = $this->following_model->count_followers($id);
		    array_push($data,array(
		                'id' => $id,
		                'name' => $info->name,
		                'email' => $info->email,
		                'gender' => $info->gender,
		                'following' => $following_data,
		                'follower' => $follower_data,
		                'song_name' => $song[0]['name'],
		                'song_artist' => $song[0]['artist']
		                ));
		                
		 }
		 
		 $this->response(json_encode($data));
	}
	
	/**
	 * list the followers which user following
	 * @param  [type] $id [user_id]
	 * @return [type]     [description]
	 */
	public function list_followers_get($id){
		$data = json_encode($this->following_model->get($id));
			$this->response($data);
	}
	
	/**
	 * list the people who is followed by user
	 * @param  [type] $id [following_id]
	 * @return [type]     [description]
	 */
	public function list_followed_get($id){
		$data = json_encode($this->following_model->get(NULL,$id));
		$this->response($data);
	}

	public function follow_get($user_id, $following_id)
	{
		$data = array(
			'user_id' => $user_id,
			'following_id' => $following_id
			);

		if(!$this->following_model->add($data))
		{
			$this->response('Failed');
		}
		else
		{
			$this->response('Success');
		}
	}
	
	
	public function share_feed_get(){
		 $raw_info = $this->message_model->get_all();
		 $data = array();
		 foreach($raw_info as $info){
		    $user_data = $this->user_model->get($info->sender_id);
		    $song_data = $this->song_model->get($info->song_id);
		    array_push($data,array('name' => $user_data[0]->name,
		                'song_name' => $song_data[0]['name'],
		                'song_lyrics' => $song_data[0]['lyrics'],
		                'song_artist' => $song_data[0]['artist'],
		                'song_genre' => $song_data[0]['genre']));
		                
		 }
		 
		 $this->response(json_encode($data));
	
	}


}


?>