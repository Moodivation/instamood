<?php

class Dictionary_model extends CI_Model
{	
	// Get the specific user data for login purpose.
	public function get($word = NULL, $mood = NULL)
	{
		if($word == NULL)
		{
			$this->db->where('mood', $mood);
			$query = $this->db->get('mood_dictionary');
		}
		else
		{
			$this->db->where('word', $word);
			$query = $this->db->get('mood_dictionary');
		}
		
		// Check the id whether is in the database
		if($query->num_rows == 1)
		{
			return $query->result();
			/*$data = array(
				'id'         =>	$row[0]['empID'],
				'name'       => $row[0]['name'],
				'password'   => $row[0]['password'],
				'last_login' => $row[0]['lastLogin'],
				'isSuccess'  => true
			);*/
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_all()
	{
		$this->db->select('*');
		$query = $this->db->get('mood_dictionary');

		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
		{
			return $query->result();
		}
	}

	public function add($data)
	{
		if(!$this->db->insert('mood_dictionary', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if(!$this->db->delete('mood_dictionary')) 
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if(!$this->db->update('mood_dictionary', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}
