<?php

class Song_model extends CI_Model
{	
	// Get the specific user data for login purpose.
public function get($id = NULL, $mood = NULL)
	{
		if($id == NULL)
		{
			$this->db->select('*');
			$this->db->where('mood', $mood);
			$query = $this->db->get('song');
		}
		else
		{
			$this->db->where('id', $id);
			$query = $this->db->get('song');
		}
		
		// Check the id whether is in the database
		if($query->num_rows != 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_all()
	{
		$this->db->select('*');
		$query = $this->db->get('song');

		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
		{
			return $query->result();
		}
	}

	public function add($data)
	{
		if(!$this->db->insert('song', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if(!$this->db->delete('song')) 
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if(!$this->db->update('song', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}


}
