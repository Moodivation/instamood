<?php

class User_model extends CI_Model
{	
	// Get the specific user data for login purpose.
	public function get($id = NULL, $username = NULL)
	{
		if($id == NULL)
		{
			$this->db->where('username', $username);
			$query = $this->db->get('user');
		}
		else
		{
			$this->db->where('id', $id);
			$query = $this->db->get('user');
		}
		
		
		// Check the id whether is in the database
		if($query->num_rows == 1)
		{
			return $query->result();
			/*$data = array(
				'id'         =>	$row[0]['empID'],
				'name'       => $row[0]['name'],
				'password'   => $row[0]['password'],
				'last_login' => $row[0]['lastLogin'],
				'isSuccess'  => true
			);*/
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_all()
	{
		$this->db->select('*');
		$query = $this->db->get('user');

		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
		{
			return $query->result();
		}
	}

	public function add($data)
	{
		if(!$this->db->insert('user', $data)) 
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if(!$this->db->delete('user')) 
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if(!$this->db->update('song', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}
