<?php

class Message_model extends CI_Model
{	
	// Get the specific user data for login purpose.
	public function get($id)
	{
		$this->db->where('$id', $id);
		$query = $this->db->get('message');
		
		// Check the id whether is in the database
		if($query->num_rows == 1)
		{
			return $query->result();
			/*$data = array(
				'id'         =>	$row[0]['empID'],
				'name'       => $row[0]['name'],
				'password'   => $row[0]['password'],
				'last_login' => $row[0]['lastLogin'],
				'isSuccess'  => true
			);*/
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_all()
	{
		$this->db->select('*');
		$query = $this->db->get('message');

		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
		{
			return $query->result();
		}
	}

	public function add($data)
	{
		if(!$this->db->insert('message', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if(!$this->db->delete('message'))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if(!$this->db->update('message', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

		public function get_song($id)
	{
		$this->db->where('sender_id', $id);
		$query = $this->db->get('message');

		return $query->result_array();

	}
}
