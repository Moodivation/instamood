<?php

class Following_model extends CI_Model
{	
	/**
	 * used to find the followers
	 * @param  [type] $user_id [the user id which is logging in]
	 * @return [type]          [description]
	 */
	public function get($user_id = NULL,$following_id = NULL)
	{
		if($user_id == NULL){
			
			$this->db->where('following_id', $following_id);
			$query = $this->db->get('following');
			
		}else{
		
			$this->db->where('user_id', $user_id);
			$query = $this->db->get('following');
		
		}
		
		// Check the id whether is in the database
		if($query->num_rows != 0)
		{
			return $query->result();
			/*$data = array(
				'id'         =>	$row[0]['empID'],
				'name'       => $row[0]['name'],
				'password'   => $row[0]['password'],
				'last_login' => $row[0]['lastLogin'],
				'isSuccess'  => true
			);*/
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_all()
	{
		$this->db->select('*');
		$query = $this->db->get('following');
		if($query->num_rows == 0)
		{
			return FALSE;
		}
		else
		{
			return $query->result();
		}
	}

	public function add($data)
	{
		if(!$this->db->insert('following', $data)){
			return false;
		} 
		return true;
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		if(!$this->db->delete('following'))
		{
			return FALSE;
		} 
		else
		{
			return TRUE;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if(!$this->db->update('following', $data))
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function count_following($user_id)
	{
		$this->db->where('user_id', $user_id);
		return $this->db->count_all_results('following');
	}

	public function count_followers($following_id)
	{
		$this->db->where('following_id', $following_id);
		return $this->db->count_all_results('following');
	}	
}
